package com.rajivprab.daily_reddit.main;

import com.rajivprab.daily_reddit.TestBase;
import org.junit.Test;

import java.util.stream.IntStream;

import static com.google.common.truth.Truth.assertThat;

public class UtilsTest extends TestBase {
    @Test
    public void shouldPost_4posts12iterations_shouldBeRoughly33Percent() {
        long numPassed = IntStream.range(0, 1000)
                                  .filter(i -> Utils.shouldPost(4, 12))
                                  .count();
        assertThat(numPassed).isAtLeast(280L);
        assertThat(numPassed).isLessThan(385L);
    }

    @Test
    public void truncate_GoodLength_shouldReturnOriginalWithTrim() {
        String title = "In defense of the surge!";
        assertThat(Utils.trim("  " + title + "  ", 80)).isEqualTo(title);
    }

    @Test
    public void truncate_tooLong_shouldTruncate() {
        String title = "Gretchen Harris likes the small brick house she bought in Norman, Okla., 36 years";
        String truncated = "Gretchen Harris likes the small brick house she bought in Norman, Okla.,  (cont)";
        assertThat(truncated).hasLength(80);
        assertThat(Utils.trim(title, 80)).isEqualTo(truncated);
    }

}
