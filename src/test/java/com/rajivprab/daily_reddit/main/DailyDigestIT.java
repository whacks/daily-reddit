package com.rajivprab.daily_reddit.main;

import com.google.common.collect.ImmutableList;
import com.rajivprab.daily_reddit.TestBase;
import com.rajivprab.daily_reddit.subscription.HNSubscription;
import com.rajivprab.daily_reddit.subscription.NYTSubscription;
import com.rajivprab.daily_reddit.subscription.RedditSubscription;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Integration test for the top-level runner.
 *
 * Check email after running this, to see that the generated email was
 * successfully sent, looks fine visually, and links are clickable.
 *
 * All emails sent are also BCCed to thecaucusnet+reddit_daily_digest@gmail.com
 *
 * Created by rajivprab on 5/19/17.
 */
public class DailyDigestIT extends TestBase {
    @Test
    public void runMain_fullIntegrationTest_usingQAResponsesFromGoogleSheet() {
        DailyDigest.main();
    }

    @Test
    public void runBadSubs() {
        DailyDigest.run("thecaucusnet+testing_runnerit@gmail.com",
                        ImmutableList.of(getRedditSub("lawyers"), getRedditSub("5916915p"),
                                    getRedditSub("SubNameIsWayTooLongIllegal"), getRedditSub("tifu")));
    }

    @Ignore
    public void runPrettyEmailForScreenshot() {
        DailyDigest.run("whackri@gmail.com", ImmutableList.of(
                NYTSubscription.getMiniInstance(),
                new HNSubscription(5),
                new RedditSubscription("Programming", 5),
                new RedditSubscription("Economics", 5)
        ));
    }

    @Test
    public void runDummyMain_forUseInHerokuWeb() {
        Dummy.main();
    }
}
