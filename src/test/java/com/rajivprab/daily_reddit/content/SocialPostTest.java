package com.rajivprab.daily_reddit.content;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.truth.Truth;
import com.rajivprab.daily_reddit.TestBase;
import com.rajivprab.daily_reddit.clients.RedditClient;
import org.json.JSONObject;
import org.junit.Test;
import org.rajivprab.cava.FileUtilc;

import java.time.Duration;
import java.time.Instant;
import java.util.Collections;
import java.util.List;

/**
 * Unit tests for the Post data structure
 *
 * Created by rajivprab on 5/18/17.
 */
public class SocialPostTest extends TestBase {
    private static final JSONObject TOP_RESULTS = new JSONObject(FileUtilc.readClasspathFile("top_results.json"));
    private static final List<SocialPost> POSTS = RedditClient.parseResponse(TOP_RESULTS);

    @Test
    public void parseTopPost() {
        SocialPost topPost = POSTS.get(0);
        Truth.assertThat(topPost.getPublishDate()).isEqualTo(Instant.parse("2017-05-14T13:15:49Z"));
        Truth.assertThat(topPost.getScoreDisplay()).isEqualTo(98299);
        Truth.assertThat(topPost.getTitle()).isEqualTo("This is democracy manifest.");
        Truth.assertThat(topPost.getUrl()).isEqualTo("http://i.imgur.com/6mgWoSH.jpg");
        Truth.assertThat(topPost.getCommentsUrl())
             .isEqualTo("https://www.reddit.com/r/pics/comments/6b3u3h/this_is_democracy_manifest/");
    }

    @Test
    public void checkThirdPost() {
        SocialPost post = POSTS.get(2);
        Truth.assertThat(post.getScoreDisplay()).isEqualTo(11934);
        Truth.assertThat(post.getTitle()).isEqualTo("My friend dressed up as the wall");
    }

    @Test
    public void getTopPostsWithTimeFilter() {
        List<SocialPost> top = SocialPost.getTop(POSTS, Instant.ofEpochSecond(1494767749), Instant.ofEpochSecond(1494807556));
        Truth.assertThat(top.subList(0, 3)).containsExactly(POSTS.get(4), POSTS.get(5), POSTS.get(7));
    }

    @Test
    public void getTopPosts_shouldOrderByScoreDesc() {
        List<SocialPost> scrambled = Lists.newArrayList(POSTS);
        Collections.shuffle(scrambled);

        List<SocialPost> topPosts = SocialPost.getTop(scrambled, Instant.now().minus(Duration.ofDays(3650)), Instant.now());
        List<Integer> topScores = topPosts.stream().map(SocialPost::getScoreDisplay).collect(ImmutableList.toImmutableList());

        List<Integer> copy = Lists.newArrayList(topScores);
        Collections.sort(copy);
        Collections.reverse(copy);

        Truth.assertThat(topScores).containsExactlyElementsIn(copy).inOrder();
        Truth.assertThat(topScores.get(0)).isGreaterThan(topScores.get(1));
    }
}
