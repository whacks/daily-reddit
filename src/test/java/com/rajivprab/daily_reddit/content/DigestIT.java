package com.rajivprab.daily_reddit.content;

import com.google.common.truth.Truth;
import com.rajivprab.daily_reddit.TestBase;
import com.rajivprab.daily_reddit.content.Digest;
import com.rajivprab.daily_reddit.subscription.HNSubscription;
import com.rajivprab.daily_reddit.subscription.RedditSubscription;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Integration tests for the email-generator.
 * No credentials needed, but does access Reddit
 *
 * Created by rajivprab on 6/12/17.
 */
public class DigestIT extends TestBase {
    @Test
    public void subDoesNotExist_shouldInformUserWithoutError() {
        String digest = generate("fibqwkqllqfg", "TiFu");
        Truth.assertThat(digest).contains("/r/TiFu");
        Truth.assertThat(digest).contains("TIFU by");
        Truth.assertThat(digest).contains("/r/fibqwkqllqfg");
        Truth.assertThat(digest).contains("No posts found. Did you make a typo?");
    }

    @Test
    public void weirdInactiveSub() {
        String digest = generate("111", "TiFu");
        Truth.assertThat(digest).contains("/r/TiFu");
        Truth.assertThat(digest).contains("TIFU by");
        Truth.assertThat(digest).contains("/r/111");
    }

    @Test
    public void illegalSubName_shouldInformUserWithoutError() {
        String digest = generate("thisdoesnotexistlalala", "tifu");
        Truth.assertThat(digest).contains("/r/tifu");
        Truth.assertThat(digest).contains("TIFU by");
        Truth.assertThat(digest).contains("/r/thisdoesnotexistlalala");
        Truth.assertThat(digest).contains("Sub does not exist");
    }

    @Test
    public void privateSub_shouldInformUserWithoutError() {
        String digest = generate("lawyers", "tifu");
        Truth.assertThat(digest).contains("/r/lawyers");
        Truth.assertThat(digest).contains("Sub is private. Unable to access it.");
        Truth.assertThat(digest).contains("/r/tifu");
        Truth.assertThat(digest).contains("TIFU by");
    }

    @Test
    public void shouldNotReuseCacheForDifferentSizedSubscriptions_onHN() {
        long startSize = Digest.getCacheSize();
        Truth.assertThat(Digest.generate(new HNSubscription(7))).contains("Hacker News");
        Truth.assertThat(Digest.generate(new HNSubscription(8))).contains("Hacker News");
        Truth.assertThat(Digest.generate(new RedditSubscription("tifu", 7))).contains("/r/tifu");
        Truth.assertThat(Digest.generate(new RedditSubscription("tifu", 8))).contains("/r/tifu");
        Truth.assertThat(Digest.getCacheSize()).isEqualTo(startSize + 4);
    }

    @Test
    public void shouldReuseCacheWherePossible() {
        long startSize = Digest.getCacheSize();
        String sub = RandomStringUtils.randomAlphanumeric(20);
        Truth.assertThat(sub).isNotEqualTo(sub.toLowerCase());

        Truth.assertThat(generate(sub)).contains("/r/" + sub);
        Truth.assertThat(Digest.generate(new HNSubscription(6))).contains("Hacker News");
        Truth.assertThat(Digest.getCacheSize()).isEqualTo(startSize + 2);

        String body = generate(sub, sub.toLowerCase(), sub.toUpperCase());
        Truth.assertThat(body).contains("/r/" + sub);
        Truth.assertThat(body).contains("/r/" + sub.toLowerCase());
        Truth.assertThat(body).contains("/r/" + sub.toUpperCase());
        Truth.assertThat(Digest.generate(new HNSubscription(6))).contains("Hacker News");
        Truth.assertThat(Digest.getCacheSize()).isEqualTo(startSize + 2);
    }

    private static String generate(String... subs) {
        return Digest.generate(
                Arrays.stream(subs).map(sub -> new RedditSubscription(sub, 10)).collect(Collectors.toList()));
    }
}
