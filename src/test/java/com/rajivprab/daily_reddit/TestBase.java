package com.rajivprab.daily_reddit;

import com.rajivprab.daily_reddit.subscription.RedditSubscription;
import com.rajivprab.daily_reddit.services.Toolbox;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.BeforeClass;
import org.rajivprab.sava.events.BufferedPublisher;
import org.rajivprab.sava.events.Publisher;

/**
 * Common set of test-infrastructure
 *
 * Created by rajivprab on 5/19/17.
 */
public class TestBase {
    protected static final Log log = LogFactory.getLog(TestBase.class);
    private static final BufferedPublisher PUBLISHER = Publisher.getBuffered();

    @BeforeClass
    public static void setupTest() {
        Toolbox.setQA(PUBLISHER);
        PUBLISHER.clear();
    }

    protected static RedditSubscription getRedditSub(String sub) {
        return new RedditSubscription(sub, 10);
    }
}
