package com.rajivprab.daily_reddit.clients;

import com.google.common.base.Joiner;
import com.google.common.truth.Truth;
import com.rajivprab.daily_reddit.TestBase;
import com.rajivprab.daily_reddit.content.SocialPost;
import org.json.JSONArray;
import org.junit.Test;

import java.time.Duration;
import java.time.Instant;
import java.util.List;

/**
 * Connects directly to HN API. No credentials needed to run.
 *
 * Created by rajivprab on 7/18/17.
 */
public class HNClientIT extends TestBase {
    @Test
    public void getTopIDs() {
        JSONArray ids = HNClient.getPostIDs();
        log.info(ids);
        Truth.assertThat(ids.length()).isGreaterThan(300);
    }

    @Test
    public void getPost() {
        SocialPost post = HNClient.getPost(14797616);
        Truth.assertThat(post.getScoreDisplay()).isGreaterThan(600);
        Truth.assertThat(post.getScoreDisplay()).isLessThan(800);
        Truth.assertThat(post.getTitle()).isEqualTo("The Myth of Drug Expiration Dates");
        Truth.assertThat(post.getCommentsUrl()).isEqualTo("https://news.ycombinator.com/item?id=14797616");
        Truth.assertThat(post.getUrl())
             .isEqualTo("https://www.propublica.org/article/the-myth-of-drug-expiration-dates");
    }

    @Test
    public void getPost_AskHN_commentsAndPostURLShouldMatch() {
        SocialPost post = HNClient.getPost(14841939);
        Truth.assertThat(post.getUrl()).isEqualTo(post.getCommentsUrl());
    }

    @Test
    public void getTopPostsTwice_shouldReuseCachedData() {
        getTopPosts();
        Instant start = Instant.now();
        getTopPosts();
        Truth.assertThat(Duration.between(start, Instant.now())).isLessThan(Duration.ofSeconds(5));
    }

    private static void getTopPosts() {
        List<SocialPost> posts = HNClient.getPosts();
        log.info("Posts:\n" + Joiner.on("\n").join(posts));
        Truth.assertThat(posts.size()).isGreaterThan(300);
        Truth.assertThat(posts.get(0).getScoreDisplay()).isGreaterThan(20);
    }
}
