package com.rajivprab.daily_reddit.clients;

import com.google.common.base.Joiner;
import com.google.common.truth.Truth;
import com.rajivprab.daily_reddit.TestBase;
import com.rajivprab.daily_reddit.clients.RedditClient.SubDoesNotExistException;
import com.rajivprab.daily_reddit.clients.RedditClient.SubIsPrivateException;
import com.rajivprab.daily_reddit.clients.RedditClient.TopDuration;
import com.rajivprab.daily_reddit.content.SocialPost;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.rajivprab.cava.Validatec;

import java.time.Duration;
import java.time.Instant;
import java.util.List;

/**
 * Integration tests for RedditInterface. No credentials needed, but does access Reddit
 *
 * Created by rajivprab on 5/11/17.
 */
public class RedditClientIT extends TestBase {
    // TODO Enhancement: Also test for month/year/all durations
    @Test
    public void baseTest_noSleep_shouldFetchTopPostsFromPast_notJustRecentDay_commentsShouldMatchPostURL() {
        Instant end = Instant.now().minus(Duration.ofDays(4));
        Instant start = end.minus(Duration.ofDays(1));
        List<SocialPost> posts = SocialPost.getTop(getAndCheckDefaultSize("nosleep"), start, end);
        Truth.assertThat(posts.size()).isAtLeast(10);
        Truth.assertThat(posts.get(0).getScoreDisplay()).isGreaterThan(1000);
        Truth.assertThat(posts.get(0).getUrl()).isEqualTo(posts.get(0).getCommentsUrl());
    }

    @Test
    public void getFromYearAgo_multiplePages_shouldMergeSuccessfully() {
        int maxSize = 365 * 2;
        List<SocialPost> posts = RedditClient.getTopPosts("programming", TopDuration.YEAR, maxSize);
        Validatec.size(posts, maxSize);
        log.info(Joiner.on("\n").join(posts));
    }

    @Test
    public void runManySubs_shouldNotTriggerRedditThrottle_commentsShouldNotMatchPostURL() {
        long startingSize = RedditClient.getCacheSize();

        getAndCheckDefaultSize("todayilearned");
        getAndCheckDefaultSize("all");
        getAndCheckDefaultSize("funny");
        getAndCheckDefaultSize("videos");
        List<SocialPost> pics = getAndCheckDefaultSize("pics");

        Truth.assertThat(RedditClient.getCacheSize()).isEqualTo(startingSize + 5);
        Truth.assertThat(pics.get(0).getUrl()).isNotEqualTo(pics.get(0).getCommentsUrl());
    }

    @Test
    public void runSameSubManyTimes_shouldReuseCachedResult() {
        long startingSize = RedditClient.getCacheSize();
        String sub = "AskReddit";

        Instant start = Instant.now();
        getAndCheckDefaultSize(sub);
        Instant cached = Instant.now();
        Truth.assertThat(RedditClient.getCacheSize()).isEqualTo(startingSize + 1);

        getAndCheckDefaultSize(sub);
        getAndCheckDefaultSize(sub.toLowerCase());
        getAndCheckDefaultSize(sub.toUpperCase());
        Instant end = Instant.now();

        Truth.assertThat(RedditClient.getCacheSize()).isEqualTo(startingSize + 1);
        Truth.assertThat(Duration.between(start, cached)).isGreaterThan(Duration.between(cached, end).multipliedBy(2));
    }

    @Test (expected = SubDoesNotExistException.class)
    public void longSubName_shouldThrowSubDoesNotExistException() {
        RedditClient.getTopPosts("thisdoesnotexistlalala");
    }

    @Test (expected = SubIsPrivateException.class)
    public void privateSub_shouldThrowSubIsPrivateException() {
        RedditClient.getTopPosts("lawyers");
    }

    @Test
    public void subDoesNotExist_doesNotThrowException() {
        List<SocialPost> response = RedditClient.getTopPosts(RandomStringUtils.randomAlphanumeric(20));
        Truth.assertThat(response).isEmpty();
    }

    private static List<SocialPost> getAndCheckDefaultSize(String sub) {
        return getAndCheckDefaultSize(sub, TopDuration.WEEK);
    }

    private static List<SocialPost> getAndCheckDefaultSize(String sub, TopDuration duration) {
        List<SocialPost> posts = RedditClient.getTopPosts(sub, duration);
        Truth.assertThat(posts).hasSize(100);
        Truth.assertThat(posts).containsNoDuplicates();
        return posts;
    }
}
