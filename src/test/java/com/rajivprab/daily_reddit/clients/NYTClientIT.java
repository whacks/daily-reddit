package com.rajivprab.daily_reddit.clients;

import com.google.common.base.Joiner;
import com.rajivprab.daily_reddit.TestBase;
import com.rajivprab.daily_reddit.clients.NYTClient.Period;
import com.rajivprab.daily_reddit.content.Post;
import org.junit.Test;

import java.util.List;

public class NYTClientIT extends TestBase {
    @Test
    public void getMostEmailedDay() {
        print(NYTClient.MOST_EMAILED_LAST_DAY);
    }

    @Test
    public void getMostFacebookedDay() {
        print(NYTClient.MOST_FACEBOOKED_LAST_DAY);
    }

    @Test
    public void getMostViewedWeek() {
        print(NYTClient.getPosts(NYTClient.getUrlMostViewed(Period.WEEK)));
    }

    private static void print(List<Post> posts) {
        log.info(Joiner.on("\n").join(posts));
    }
}
