package com.rajivprab.daily_reddit.subscription;

import com.rajivprab.daily_reddit.TestBase;
import com.rajivprab.daily_reddit.subscription.HNSubscription;
import org.junit.Test;

import java.time.Duration;
import java.time.Instant;

/**
 * Created by rajivprab on 7/20/17.
 */
public class HNSubscriptionIT extends TestBase {
    @Test
    public void getContent() {
        log.info(new HNSubscription(10).getContent(
                Instant.now().minus(Duration.ofDays(2)), Instant.now().minus(Duration.ofDays(1))));
    }
}
