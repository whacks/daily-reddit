package com.rajivprab.daily_reddit.subscription;

import com.rajivprab.daily_reddit.TestBase;
import com.rajivprab.daily_reddit.clients.NYTClient;
import com.rajivprab.daily_reddit.content.SocialPost;
import org.junit.Test;

import java.time.Instant;
import java.util.List;

import static com.google.common.truth.Truth.assertThat;

public class NYTSubscriptionIT extends TestBase {
    @Test
    public void getEntries() {
        log.info(NYTSubscription.INSTANCE.getContent(null, null));
    }

    @Test
    public void shouldBeSortedByDescendingRank_andIgnoreGivenTimes() {
        String content = NYTSubscription.INSTANCE.getContent(Instant.now(), Instant.now());
        int rank1 = content.indexOf("[1]");
        int rank2 = content.indexOf("[2]");
        int rank3 = content.indexOf("[3]");
        int rank4 = content.indexOf("[4]");
        assertThat(rank1).isEqualTo(0);
        assertThat(rank1).isLessThan(rank2);
        assertThat(rank2).isLessThan(rank3);
        assertThat(rank3).isLessThan(rank4);
    }

    @Test
    public void postsShouldBeSortedByRank_notScore() {
        List<SocialPost> posts = SocialPost.getTop(NYTClient.MOST_POPULAR);
        for (int i=0; i<posts.size(); i++) {
            assertThat(posts.get(i).getScoreDisplay()).isEqualTo(i+1);
        }
    }
}
