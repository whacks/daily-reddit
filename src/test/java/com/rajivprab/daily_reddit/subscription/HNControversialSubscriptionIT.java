package com.rajivprab.daily_reddit.subscription;

import com.rajivprab.daily_reddit.TestBase;
import com.rajivprab.daily_reddit.subscription.HNControversialSubscription;
import org.junit.Test;

import java.time.Duration;
import java.time.Instant;

public class HNControversialSubscriptionIT extends TestBase {
    @Test
    public void getControversialContent() {
        log.info(new HNControversialSubscription(10).getContent(
                Instant.now().minus(Duration.ofDays(2)), Instant.now().minus(Duration.ofDays(1))));
    }
}
