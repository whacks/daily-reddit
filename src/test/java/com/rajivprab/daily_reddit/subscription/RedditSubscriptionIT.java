package com.rajivprab.daily_reddit.subscription;

import com.google.common.truth.Truth;
import com.rajivprab.daily_reddit.TestBase;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;

import java.time.Duration;
import java.time.Instant;

/**
 * Integration test. Connects to Reddit API but no credentials needed
 *
 * Created by rajivprab on 7/22/17.
 */
public class RedditSubscriptionIT extends TestBase {
    private static final Log log = LogFactory.getLog(RedditSubscriptionIT.class);
    private static final String COMMENTS_MARKER = "comments</a></small></i><br>";

    @Test
    public void getPhilosophy_shouldHaveComments() {
        Instant endTime = Instant.now().minus(Duration.ofDays(1));
        Instant startTime = endTime.minus(Duration.ofDays(1)).minus(Duration.ofMinutes(5));
        String content = new RedditSubscription("philosophy", 10).getContent(startTime, endTime);
        log.info(content);
        Truth.assertThat(content).contains(COMMENTS_MARKER);
    }

    @Test
    public void getNoSleep_shouldNotHaveComments() {
        Instant endTime = Instant.now().minus(Duration.ofDays(1));
        Instant startTime = endTime.minus(Duration.ofDays(1)).minus(Duration.ofMinutes(5));
        String content = new RedditSubscription("nosleep", 10).getContent(startTime, endTime);
        log.info(content);
        Truth.assertThat(content).doesNotContain(COMMENTS_MARKER);
    }
}
