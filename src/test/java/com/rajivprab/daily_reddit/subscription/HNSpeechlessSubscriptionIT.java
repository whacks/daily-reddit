package com.rajivprab.daily_reddit.subscription;

import com.rajivprab.daily_reddit.TestBase;
import com.rajivprab.daily_reddit.subscription.HNSpeechlessSubscription;
import org.junit.Test;

import java.time.Duration;
import java.time.Instant;

public class HNSpeechlessSubscriptionIT extends TestBase {
    @Test
    public void getSpeechlessContent() {
        log.info(new HNSpeechlessSubscription(10).getContent(
                Instant.now().minus(Duration.ofDays(2)), Instant.now().minus(Duration.ofDays(1))));
    }
}
