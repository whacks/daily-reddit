package com.rajivprab.daily_reddit.services;

import com.rajivprab.daily_reddit.TestBase;
import org.json.JSONObject;
import org.junit.Test;
import org.rajivprab.cava.FileUtilc;

import static com.google.common.truth.Truth.assertThat;

/**
 * Unit tests for user-signups module
 *
 * Created by rajivprab on 7/5/17.
 */
public class UserSignUpsTest extends TestBase {
    @Test
    public void shouldParseFromResponse_withDuplicateResponseAndDuplicateSub() {
        JSONObject json = new JSONObject(FileUtilc.readClasspathFile("form_responses.json"));
        UserSignUps responses = new UserSignUps(json);
        assertThat(responses.getUsers()).containsExactly("testing@gmail.com");
        assertThat(responses.getSubs("testing@gmail.com")).containsExactly(
                getRedditSub("wholesomememes"), getRedditSub("UpliftingNews"), getRedditSub("tifu"),
                getRedditSub("philosophy"), getRedditSub("programming")).inOrder();
    }

    @Test
    public void isValidEmail() {
        assertThat(UserSignUps.isValidEmail("random+123_abcEWD@gmail.edu")).isTrue();
    }

    @Test
    public void isInvalidEmail_noAckSign() {
        assertThat(UserSignUps.isValidEmail("random+123_abcEWDgmail.edu")).isFalse();
    }

    @Test
    public void isInvalidEmail_noPeriodAtEnd() {
        assertThat(UserSignUps.isValidEmail("random+123_abcEWD@gmailedu")).isFalse();
    }
}
