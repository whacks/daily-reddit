package com.rajivprab.daily_reddit.services;

import com.rajivprab.daily_reddit.TestBase;
import com.rajivprab.daily_reddit.subscription.HNSpeechlessSubscription;
import com.rajivprab.daily_reddit.subscription.HNSubscription;
import com.rajivprab.daily_reddit.subscription.NYTSubscription;
import org.junit.Test;

import static com.google.common.truth.Truth.assertThat;

/**
 * Integration tests for FormResponse
 * <p>
 * Created by rajivprab on 6/11/17.
 */
public class UserSignUpsIT extends TestBase {
    @Test
    public void shouldGetResponses() {
        UserSignUps signUps = UserSignUps.get();
        String user1 = "thecaucusnet+testing_reddit_digest@gmail.com";
        String user2 = "thecaucusnet+testing_reddit_digest2@gmail.com";
        String user3 = "thecaucusnet+testing_reddit_digest3@gmail.com";
        String user4 = "thecaucusnet+testing_reddit_digest4@gmail.com";

        assertThat(signUps.getUsers()).containsExactly(user1, user2, user3, user4);
        assertThat(signUps.getSubs(user1))
             .containsExactly(new HNSpeechlessSubscription(10), new HNSubscription(10),
                              getRedditSub("nosleep"), getRedditSub("UpliftingNews"),
                              getRedditSub("thisdoesnotexistlalala"), getRedditSub("the_donald"),
                              getRedditSub("111"), getRedditSub("wholesomememes")).inOrder();

        assertThat(signUps.getSubs(user2))
                .containsExactly(new HNSpeechlessSubscription(10), new HNSubscription(10));

        assertThat(signUps.getSubs(user3))
             .containsExactly(getRedditSub("WholesomeMemes"),
                              getRedditSub("HumansBeingBros"), getRedditSub("UpliftingNews"), getRedditSub("TIFU"),
                              getRedditSub("All"), getRedditSub("PersonalFinance"), getRedditSub("TED"),
                              getRedditSub("AskHistorians"), getRedditSub("Philosophy"), getRedditSub("Economics"),
                              getRedditSub("Psychology"), getRedditSub("ShortScaryStories"), getRedditSub("NoSleep"))
             .inOrder();

        assertThat(signUps.getSubs(user4))
                .containsExactly(NYTSubscription.INSTANCE,
                                 new HNSpeechlessSubscription(10), new HNSubscription(10),
                                 getRedditSub("WholesomeMemes"), getRedditSub("HumansBeingBros"),
                                 getRedditSub("UpliftingNews"), getRedditSub("TIFU"), getRedditSub("All"),
                                 getRedditSub("PersonalFinance"), getRedditSub("TED"), getRedditSub("AskHistorians"),
                                 getRedditSub("Philosophy"), getRedditSub("Economics"), getRedditSub("Psychology"),
                                 getRedditSub("ShortScaryStories"), getRedditSub("NoSleep"))
                .inOrder();
    }
}
