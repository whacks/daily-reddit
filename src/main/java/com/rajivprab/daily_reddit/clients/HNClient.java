package com.rajivprab.daily_reddit.clients;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import com.rajivprab.daily_reddit.content.SocialPost;
import com.rajivprab.daily_reddit.services.Toolbox;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.rajivprab.cava.IOUtilc;
import org.rajivprab.cava.JsonUtilc;
import org.rajivprab.cava.ThreadUtilc;
import org.rajivprab.cava.Validatec;
import org.rajivprab.cava.exception.IOExceptionc;

import javax.ws.rs.core.Response.Status;
import java.io.IOException;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

/**
 * Dumb client for interacting with the HN API
 *
 * Created by rajivprab on 7/18/17.
 */
public class HNClient {
    private static final Logger log = Toolbox.log(HNClient.class);

    private static final String DOMAIN = "https://hacker-news.firebaseio.com/v0/";
    private static final HttpClient CLIENT = HttpClients.createDefault();

    public static List<SocialPost> getPosts() {
        return PostsHolder.POSTS;
    }

    @VisibleForTesting
    static JSONArray getPostIDs() {
        return sendRequest("topstories.json");
    }

    @VisibleForTesting
    static SocialPost getPost(long id) {
        return parseHNResponse(sendRequest("item/" + id + ".json"));
    }

    private static SocialPost parseHNResponse(JSONObject response) {
        try {
            return SocialPost.withScore(Instant.ofEpochSecond(response.getLong("time")),
                                        response.getString("title"),
                                        getHNPostUrl(response),
                                        getHNCommentsUrl(response),
                                        response.getInt("score"),
                                        response.optInt("descendants"));
        } catch (RuntimeException e) {
            log.error("Unable to parse: " + response.toString(2));
            throw e;
        }
    }

    private static String getHNPostUrl(JSONObject response) {
        return response.has("url") ? response.getString("url") : getHNCommentsUrl(response);
    }

    private static String getHNCommentsUrl(JSONObject response) {
        return "https://news.ycombinator.com/item?id=" + response.getLong("id");
    }

    private static <T> T sendRequest(String urlSuffix) {
        try {
            HttpResponse response = CLIENT.execute(new HttpGet(DOMAIN + urlSuffix));
            int statusCode = response.getStatusLine().getStatusCode();
            String responseBody = IOUtilc.toString(response.getEntity().getContent());
            Validatec.equals(statusCode, Status.OK.getStatusCode());
            log.debug("Got HN data: " + responseBody);    // Producing a lot of log outputs, causing mvn to fail
            return (T) (responseBody.startsWith("{") ? new JSONObject(responseBody) : new JSONArray(responseBody));
        } catch (IOException e) {
            throw new IOExceptionc(e);
        }
    }

    // Moving this method to the PostsHolder class, causes deadlock
    private static List<SocialPost> populatePosts() {
        // TODO Enhancement why is this not scaling with number of threads?
        // return ThreadUtilc.get(new ForkJoinPool(2).submit(() -> JsonUtilc.<Integer>getStream(getPostIDs())
        // .parallel().map(HackerNewsClient::getPost).collect(Collectors.toList())));
        log.info("Populating HN posts");
        ExecutorService executor = Executors.newFixedThreadPool(10);
        try {
            return JsonUtilc.<Integer>getStream(getPostIDs())
                    .map(id -> executor.submit(() -> getPost(id)))
                    .collect(Collectors.toList()).stream()  // Getting rid of this causes sequential get-waits
                    .map(ThreadUtilc::get)
                    .collect(ImmutableList.toImmutableList());
        } finally {
            log.info("Shutting down the executor");
            executor.shutdown();
        }
    }

    private static class PostsHolder {
        // Placing this at the top level class produces deadlock
        // https://stackoverflow.com/questions/45246122/deadlock-caused-by-creating-a-new-thread-during-class-initialization
        private static final List<SocialPost> POSTS = populatePosts();
    }
}
