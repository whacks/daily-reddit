package com.rajivprab.daily_reddit.clients;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.rajivprab.daily_reddit.content.Post;
import com.rajivprab.daily_reddit.content.SocialPost;
import com.rajivprab.daily_reddit.services.Toolbox;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONObject;
import org.rajivprab.cava.IOUtilc;
import org.rajivprab.cava.JSONObjectImmutable;
import org.rajivprab.cava.JsonUtilc;
import org.rajivprab.cava.Validatec;
import org.rajivprab.cava.exception.CheckedExceptionWrapper;

import javax.ws.rs.core.Response.Status;
import java.net.URI;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * https://developer.nytimes.com/docs/most-popular-product/1/overview
 */
public class NYTClient {
    private static final JSONObject CREDENTIALS = Toolbox.getApiKeys("nytimes/nytimes.json");
    private static final String APP_ID = CREDENTIALS.getString("app-id");
    private static final String API_KEY = CREDENTIALS.getString("api-key");
    private static final String SECRET = CREDENTIALS.getString("secret");

    private static final ZoneId TIMEZONE = ZoneId.of("America/New_York");

    @VisibleForTesting static final List<Post> MOST_EMAILED_LAST_DAY = getPosts(getUrlMostEmailed(Period.DAY));
    @VisibleForTesting static final List<Post> MOST_FACEBOOKED_LAST_DAY = getPosts(getUrlMostSharedOnFacebook(Period.DAY));
    public static final List<SocialPost> MOST_POPULAR = rank(interleaveUnique(MOST_EMAILED_LAST_DAY, MOST_FACEBOOKED_LAST_DAY));

    enum Period {
        DAY(1), WEEK(7), MONTH(30);

        private final int numDays;

        Period(int numDays) {
            this.numDays = numDays;
        }
    }

    enum ShareType {
        FACEBOOK("facebook");

        private final String pathParam;

        ShareType(String pathParam) {
            this.pathParam = pathParam;
        }
    }

    @VisibleForTesting
    static List<Post> getPosts(String url) {
        try {
            URI uri = new URIBuilder(url)
                    .addParameter("api-key", API_KEY)
                    .build();
            HttpResponse response = HttpClients.createDefault().execute(new HttpGet(uri));
            String body = IOUtilc.toString(response.getEntity().getContent());
            int status = response.getStatusLine().getStatusCode();
            Validatec.equals(status, Status.OK.getStatusCode(), "Code: " + status + ", Response: " + body);
            return parseResponse(JSONObjectImmutable.build(body));
        } catch (Exception e) {
            throw CheckedExceptionWrapper.wrapIfNeeded(e);
        }
    }

    @VisibleForTesting
    static String getUrlMostEmailed(Period period) {
        return String.format("https://api.nytimes.com/svc/mostpopular/v2/emailed/%s.json", period.numDays);
    }

    @VisibleForTesting
    static String getUrlMostViewed(Period period) {
        return String.format("https://api.nytimes.com/svc/mostpopular/v2/viewed/%s.json", period.numDays);
    }

    @VisibleForTesting
    static String getUrlMostSharedOnFacebook(Period period) {
        return String.format("https://api.nytimes.com/svc/mostpopular/v2/shared/%s/%s.json",
                             period.numDays, ShareType.FACEBOOK.pathParam);
    }

    private static List<Post> parseResponse(JSONObject response) {
        Toolbox.log(NYTClient.class).info("Response: " + response.toString(2));
        return JsonUtilc.<JSONObject>getStream(response.getJSONArray("results"))
                        .map(NYTClient::parseEntry)
                        .collect(ImmutableList.toImmutableList());
    }

    private static Post parseEntry(JSONObject entry) {
        String title = entry.getString("title");
        String url = entry.getString("url");
        Instant published = LocalDate.parse(entry.getString("published_date")).atStartOfDay(TIMEZONE).toInstant();
        return Post.build(title, url, published);
    }

    private static <T extends Post> List<T> interleaveUnique(List<T> a, List<T> b) {
        List<T> interleaved = interleave(a, b);
        List<T> unique = ImmutableList.copyOf(Sets.newLinkedHashSet(interleaved));

        Set<String> uniqueTitles = Sets.newHashSet();
        a.forEach(p -> uniqueTitles.add(p.getTitle()));
        b.forEach(p -> uniqueTitles.add(p.getTitle()));
        Validatec.size(unique, uniqueTitles.size());

        return unique;
    }

    private static <T> List<T> interleave(List<T> a, List<T> b) {
        Iterator<T> aItr = a.iterator();
        Iterator<T> bItr = b.iterator();
        List<T> interleaved = Lists.newArrayList();
        while (aItr.hasNext() && bItr.hasNext()) {
            interleaved.add(aItr.next());
            interleaved.add(bItr.next());
        }
        while (aItr.hasNext()) {
            interleaved.add(aItr.next());
        }
        while (bItr.hasNext()) {
            interleaved.add(bItr.next());
        }
        return interleaved;
    }

    private static List<SocialPost> rank(List<? extends Post> posts) {
        List<SocialPost> ranked = Lists.newArrayList();
        for (int rank=1; rank<=posts.size(); rank++) {
            Post cur = posts.get(rank-1);
            ranked.add(SocialPost.withRanking(cur, rank));
        }
        Validatec.size(ranked, posts.size());
        return ImmutableList.copyOf(ranked);
    }
}
