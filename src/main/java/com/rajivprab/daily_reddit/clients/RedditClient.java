package com.rajivprab.daily_reddit.clients;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.rajivprab.daily_reddit.content.SocialPost;
import com.rajivprab.daily_reddit.main.Utils;
import com.rajivprab.daily_reddit.services.Toolbox;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.rajivprab.cava.*;

import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.net.URI;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Client for using the Reddit API
 * <p>
 * Created by rajivprab on 5/11/17.
 */
public class RedditClient {
    private static final Logger log = Toolbox.log(RedditClient.class);

    public static final String BASE_URL = "https://www.reddit.com";
    private static final int MAX_RESULTS_PER_CALL = 100;

    public enum TopDuration {
        HOUR("hour"),
        DAY("day"),
        WEEK("week"),
        MONTH("month"),
        YEAR("year"),
        ALL("all");

        private final String queryParam;

        TopDuration(String queryParam) {
            this.queryParam = queryParam;
        }

        String getQueryParam() {
            return queryParam;
        }
    }

    // TODO Enhancement: Multiple entries for the same subreddit, but different maxResults. Can reuse
    private static final Cache<RedditFetchEntry, List<SocialPost>> CACHE = CacheBuilder.newBuilder().build();

    public static List<SocialPost> getTopPosts(String subReddit) {
        return getTopPosts(subReddit, TopDuration.WEEK);
    }

    public static List<SocialPost> getTopPosts(String subReddit, TopDuration topDuration) {
        return getTopPostsCached(subReddit, topDuration, MAX_RESULTS_PER_CALL);
    }

    public static List<SocialPost> getTopPosts(String subReddit, TopDuration topDuration, int maxResults) {
        return getTopPostsCached(subReddit, topDuration, maxResults);
    }

    @VisibleForTesting
    public static List<SocialPost> parseResponse(JSONObject response) {
        log.debug("Response: " + response);     // Producing a lot of log outputs, causing mvn to fail
        return JsonUtilc.<JSONObject>getStream(response.getJSONObject("data").getJSONArray("children"))
                .map(child -> child.getJSONObject("data"))
                .map(RedditClient::parseEntry)
                .collect(Collectors.toList());
    }

    private static List<SocialPost> getTopPostsCached(String subReddit, TopDuration topDuration, int maxResults) {
        return Mapc.cacheGet(CACHE, new RedditFetchEntry(subReddit, topDuration, maxResults),
                             () -> getTopPostsMerged(subReddit, topDuration, maxResults));
    }

    private static List<SocialPost> getTopPostsMerged(String subReddit, TopDuration topDuration, int maxResults) {
        JSONObject batch = getTopPostsFromAPI(subReddit, topDuration);
        List<SocialPost> batchPosts = parseResponse(batch);
        Validatec.greaterOrEqual(MAX_RESULTS_PER_CALL, batchPosts.size());

        List<SocialPost> allPosts = Lists.newArrayList(batchPosts);

        while (batchPosts.size() == MAX_RESULTS_PER_CALL && allPosts.size() < maxResults) {
            batch = getTopPostsFromAPI(subReddit, topDuration, batch.getJSONObject("data").getString("after"));
            batchPosts = parseResponse(batch);
            allPosts.addAll(batchPosts);
        }

        return ImmutableList.copyOf(Utils.truncate(allPosts, maxResults));
    }

    private static JSONObject getTopPostsFromAPI(String subReddit, TopDuration topDuration) {
        return getTopPostsFromAPI(subReddit, topDuration, "");
    }

    private static JSONObject getTopPostsFromAPI(String subReddit, TopDuration topDuration, String after) {
        URI uri = UriBuilder.fromPath(BASE_URL + "/r/" + subReddit + "/top/.json")
                            .queryParam("t", topDuration.getQueryParam())
                            .queryParam("after", after)
                            .queryParam("limit", MAX_RESULTS_PER_CALL)
                            .build();
        HttpGet request = new HttpGet(uri);
        request.addHeader("User-agent", "Random Script 125277w");
        try {
            return launchRequest(request);
        } catch (UnknownSubFetchError e) {
            ThreadUtilc.sleep(2000);
            return launchRequest(request);
        }
    }

    private static SocialPost parseEntry(JSONObject entry) {
        String title = entry.getString("title");
        String url = entry.getString("url");
        String commentsUrl = RedditClient.BASE_URL + entry.getString("permalink");
        Instant created = Instant.ofEpochSecond(entry.getLong("created_utc"));
        int score = entry.getInt("score");
        return SocialPost.withScore(created, title, url, commentsUrl, score);
    }

    private static JSONObject launchRequest(HttpGet request) {
        try {
            // As of May 2021, getting a lot of {"Too Many Requests", "error": 429} errors from Reddit. Hence, sleep
            Utils.randomSleep(Duration.ofSeconds(8));

            // Reddit seems to reject >3 requests coming from the same client, even with sleep. Create new client each time
            HttpResponse response = HttpClients.createDefault().execute(request);
            int statusCode = response.getStatusLine().getStatusCode();
            String responseBody = IOUtilc.toString(response.getEntity().getContent());
            if (statusCode == Status.OK.getStatusCode()) {
                return new JSONObject(responseBody);
            }
            Validatec.notEquals(statusCode, Status.FORBIDDEN.getStatusCode(), SubIsPrivateException::new);
            Validatec.notEquals(statusCode, Status.NOT_FOUND.getStatusCode(), SubDoesNotExistException::new);
            throw new UnknownSubFetchError(responseBody);
        } catch (IOException e) {
            throw new UnknownSubFetchError(e);
        }
    }

    @VisibleForTesting
    static long getCacheSize() {
        return CACHE.size();
    }

    private static class RedditFetchEntry {
        private final String sub;
        private final TopDuration topDuration;
        private final int maxResults;

        private RedditFetchEntry(String sub, TopDuration topDuration, int maxResults) {
            this.sub = Validatec.notEmpty(sub.toLowerCase());
            this.topDuration = Validatec.notNull(topDuration);
            this.maxResults = maxResults;
            Validatec.greaterThan(maxResults, 0);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof RedditFetchEntry)) return false;
            RedditFetchEntry that = (RedditFetchEntry) o;
            return maxResults == that.maxResults &&
                    sub.equals(that.sub) &&
                    topDuration == that.topDuration;
        }

        @Override
        public int hashCode() {
            return Objects.hash(sub, topDuration, maxResults);
        }
    }

    public static class SubDoesNotExistException extends RuntimeException {
    }

    public static class SubIsPrivateException extends RuntimeException {
    }

    public static class UnknownSubFetchError extends RuntimeException {
        private UnknownSubFetchError(String message) {
            super(message);
        }

        private UnknownSubFetchError(Exception e) {
            super(e);
        }
    }
}
