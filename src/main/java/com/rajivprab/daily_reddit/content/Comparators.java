package com.rajivprab.daily_reddit.content;

import java.util.Comparator;

class Comparators {
    static final Comparator<SocialPost> RANKING_COMPARATOR = Comparator.comparingInt(o -> o.getRanking().get());
    static final Comparator<SocialPost> SCORE_COMPARATOR = (o1, o2) -> o2.getScore().get() - o1.getScore().get();
    static final Comparator<SocialPost> SPEECHLESS_COMPARATOR = new SpeechlessComparator();
    static final Comparator<SocialPost> BUZZ_COMPARATOR = new BuzzComparator();

    private static class SpeechlessComparator implements Comparator<SocialPost> {
        @Override
        public int compare(SocialPost one, SocialPost two) {
            return Double.compare(getSpeechlessScore(two), getSpeechlessScore(one));
        }

        private static double getSpeechlessScore(SocialPost post) {
            // Hypothesis: Posts that are more "insightful", have a higher score/comments ratio
            // Opposite of bike-shedding: Because it's so new and insightful,
            // people don't feel the need to give their 2 cents
            double ratio = post.getScore().get() * 1.0 / (post.getNumComments().get() + 1);
            // We want to filter out posts that have a very low score, but very few comments as well.
            // Eg: 10 points and 0 comments
            // Hence: multiply the ratio by the score. Get posts that are reasonably popular with high ratio
            // However, this leads to the ranking being dominated by popular posts. Hack: Cap the score at 100
            return ratio * Math.min(100, post.getScore().get());
        }
    }

    private static class BuzzComparator implements Comparator<SocialPost> {
        @Override
        public int compare(SocialPost one, SocialPost two) {
            return Double.compare(getScore(two), getScore(one));
        }

        private static double getScore(SocialPost post) {
            // The greater the ratio, the more "buzzing" a post is
            // However, the ratio rarely, if ever, exceeds 1.5
            // Hence, need to square it, in order to prevent the listing from being dominated by purely popular posts
            double ratio = post.getNumComments().get() * 1.0 / (post.getScore().get() + 1);
            return post.getScore().get() * ratio * ratio * ratio;
        }
    }
}
