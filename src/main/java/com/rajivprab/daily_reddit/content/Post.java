package com.rajivprab.daily_reddit.content;

import org.rajivprab.cava.Validatec;

import java.time.Instant;
import java.util.Objects;

public class Post {
    private final String title;
    private final String url;
    private final Instant publishDate;

    public static Post build(String title, String url, Instant publishDate) {
        return new Post(title, url, publishDate);
    }

    Post(String title, String url, Instant publishDate) {
        this.title = Validatec.notEmpty(title);
        this.url = Validatec.notEmpty(url);
        this.publishDate = Validatec.notNull(publishDate);
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public Instant getPublishDate() {
        return publishDate;
    }

    @Override
    public String toString() {
        return "Post{" +
                "title='" + title + '\'' +
                ", url='" + url + '\'' +
                ", publishDate=" + publishDate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        Post post = (Post) o;
        return title.equals(post.title) &&
                url.equals(post.url) &&
                publishDate.equals(post.publishDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, url, publishDate);
    }
}
