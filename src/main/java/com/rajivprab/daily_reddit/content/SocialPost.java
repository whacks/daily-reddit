package com.rajivprab.daily_reddit.content;

import com.google.common.collect.ImmutableList;
import org.rajivprab.cava.Validatec;

import java.time.Instant;
import java.util.*;

/**
 * Representation of a single Social Media Post
 *
 * TODO Enhancement: Split into ScoredPost and RankedPost
 * <p>
 * Created by rajivprab on 5/15/17.
 */
public class SocialPost extends Post {
    private final String commentsUrl;   // Set to same as url, if there is no comment-specific URL to use
    private final Optional<Integer> score;
    private final Optional<Integer> ranking;
    private final Optional<Integer> numComments;

    public static SocialPost withRanking(Post post, int ranking) {
        return withRanking(post.getPublishDate(), post.getTitle(), post.getUrl(), ranking);
    }

    public static SocialPost withRanking(Instant created, String title, String url, int ranking) {
        return new SocialPost(created, title, url, url,
                              Optional.empty(), Optional.of(ranking), Optional.empty());
    }

    public static SocialPost withScore(Instant created, String title, String url, String commentsUrl, int score) {
        return new SocialPost(created, title, url, commentsUrl,
                              Optional.of(score), Optional.empty(), Optional.empty());
    }

    public static SocialPost withScore(Instant created, String title, String url, String commentsUrl,
                                       int score, int numComments) {
        return new SocialPost(created, title, url, commentsUrl,
                              Optional.of(score), Optional.empty(),
                              Optional.of(numComments));
    }

    private SocialPost(Instant created, String title, String url, String commentsUrl,
                       Optional<Integer> score, Optional<Integer> ranking, Optional<Integer> numComments) {
        super(title, url, created);
        Validatec.isTrue(score.isPresent() ^ ranking.isPresent(), "Provide either Ranking or Score, not both");
        this.commentsUrl = commentsUrl;
        this.score = score;
        this.ranking = ranking;
        this.numComments = numComments;
    }

    // --------------- Getters -----------------

    public Optional<Integer> getScore() {
        return score;
    }

    public Optional<Integer> getRanking() {
        return ranking;
    }

    public int getScoreDisplay() {
        return score.orElseGet(ranking::get);
    }

    public String getCommentsUrl() {
        return commentsUrl;
    }

    public Optional<Integer> getNumComments() {
        return numComments;
    }

    private Comparator<SocialPost> getComparator() {
        return score.isPresent() ? Comparators.SCORE_COMPARATOR : Comparators.RANKING_COMPARATOR;
    }

    @Override
    public String toString() {
        return "SocialPost{" +
                "created=" + getPublishDate() +
                ", title='" + getTitle() + '\'' +
                ", url='" + getUrl() + '\'' +
                ", commentsUrl='" + commentsUrl + '\'' +
                ", score=" + score +
                ", ranking=" + ranking +
                ", numComments=" + numComments +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SocialPost)) return false;
        SocialPost that = (SocialPost) o;
        return getPublishDate().equals(that.getPublishDate()) &&
                getTitle().equals(that.getTitle()) &&
                getUrl().equals(that.getUrl()) &&
                getCommentsUrl().equals(that.getCommentsUrl()) &&
                getScore().equals(that.getScore()) &&
                getRanking().equals(that.getRanking()) &&
                getNumComments().equals(that.getNumComments());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getCommentsUrl(), getScore(), getRanking(), getNumComments());
    }

    // -------------------- Utilities ------------------

    public static List<SocialPost> getSpeechless(Collection<SocialPost> posts, Instant start, Instant end) {
        return sort(posts, start, end, Comparators.SPEECHLESS_COMPARATOR);
    }

    public static List<SocialPost> getBuzzing(Collection<SocialPost> posts, Instant start, Instant end) {
        return sort(posts, start, end, Comparators.BUZZ_COMPARATOR);
    }

    public static List<SocialPost> getTop(Collection<SocialPost> posts) {
        return getTop(posts, Instant.MIN, Instant.MAX);
    }

    public static List<SocialPost> getTop(Collection<SocialPost> posts, Instant start, Instant end) {
        if (posts.isEmpty()) { return ImmutableList.of(); }
        return sort(posts, start, end, posts.iterator().next().getComparator());
    }

    private static List<SocialPost> sort(Collection<SocialPost> posts, Instant start, Instant end, Comparator<SocialPost> comparator) {
        return getFiltered(posts, start, end)
                .stream()
                .sorted(comparator)
                .collect(ImmutableList.toImmutableList());
    }

    private static List<SocialPost> getFiltered(Collection<SocialPost> posts, Instant start, Instant end) {
        Validatec.greaterThan(end, start);
        return posts.stream()
                    .filter(post -> post.getPublishDate().isAfter(start))
                    .filter(post -> post.getPublishDate().isBefore(end))
                    .collect(ImmutableList.toImmutableList());
    }
}
