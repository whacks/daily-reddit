package com.rajivprab.daily_reddit.content;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.ImmutableList;
import com.rajivprab.daily_reddit.subscription.Subscription;
import org.rajivprab.cava.FileUtilc;
import org.rajivprab.cava.Mapc;

import java.time.Duration;
import java.time.Instant;
import java.util.Collection;

import static org.rajivprab.cava.WebUtilc.getHtmlLink;

/**
 * Static methods used for generating the digest content.
 * <p>
 * Suppose this is called once everyday at 11pm, with Duration=Day.
 * =>   Any post that was published shortly before 11pm will never show up in the results,
 * no matter how popular it ends up being.
 * <p>
 * If we try to fix this by using Duration=TwoDays, we will wind up with numerous duplicate results.
 * =>  A very popular post from 11am will show up in today's results, as well as tomorrow's results.
 * <p>
 * Solution: Use Duration=TwoDays and ignore all results that are less than 1 day old.
 * <p>
 * Created by rajivprab on 5/20/17.
 */
public class Digest {
    private static final String FOOTER = getFooter();

    // TODO Enhancement: Put size limit, and move to Subscription class?
    private static final Cache<Subscription, String> subContentCache = CacheBuilder.newBuilder().build();

    public static String generate(Subscription... userSubscriptions) {
        return generate(ImmutableList.copyOf(userSubscriptions));
    }

    public static String generate(Collection<Subscription> userSubscriptions) {
        StringBuilder body = new StringBuilder();
        userSubscriptions.forEach(sub -> body.append(generate(sub)));
        return body.append(FOOTER).toString();
    }

    private static String generate(Subscription subscription) {
        // Do not cache the header, since different users may have different capitalization for the same subreddit
        return "<h1>" + getHtmlLink(subscription.getHeader(), subscription.getHeaderUrl()) + "</h1>" +
                Mapc.cacheGet(subContentCache, subscription, () -> getContent(subscription));
    }

    private static String getContent(Subscription sub) {
        // Running on Tue 7am       => [Mon 7am,    Sun 6.55am]
        // Running on Wed 7.02am    => [Tue 7.02am, Mon 6.57am]
        // The minus-5-minutes can cause something posted at 6.58am on Monday to show up on both days
        // But it also prevents content posted at Monday 7.02am from never showing up, if Wed's run is delayed by ~3 minutes
        // This buffer duration should match the variance in Heroku's scheduler, and the run-duration of the task
        Instant endTime = Instant.now().minus(Duration.ofDays(1));
        Instant startTime = endTime.minus(Duration.ofDays(1)).minus(Duration.ofMinutes(5));
        return sub.getContent(startTime, endTime);
    }

    private static String getFooter() {
        String projectLink = "https://gitlab.com/whacks/daily-reddit";
        String formLink = "https://docs.google.com/forms/d/e/1FAIpQLSfyBQ_wEHbh00CcLotTIFQn9VwnxyoXjqkoKVO8OsP-zI2Mtg/viewform?usp=sf_link#response=ACYDBNiUtd-pQ2W2Ss4GJx8ivQT-D7yzRa9lWhpBykoeyzKJDBw8w_vHlGJxeeI";
        return FileUtilc.readClasspathFile("footer.html")
                        .replace("PROJECT_PAGE", getHtmlLink("project page", projectLink))
                        .replace("FORM_LINK", getHtmlLink("form", formLink));
    }

    @VisibleForTesting
    static long getCacheSize() {
        return subContentCache.size();
    }
}
