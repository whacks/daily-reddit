package com.rajivprab.daily_reddit.subscription;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import com.rajivprab.daily_reddit.content.SocialPost;
import com.rajivprab.daily_reddit.services.Toolbox;
import org.rajivprab.sava.logging.Severity;

import java.time.Instant;
import java.util.Collection;
import java.util.List;

import static org.rajivprab.cava.WebUtilc.getHtmlLink;

/**
 * Representation of a subscription to a single source of content (eg, HN or /r/programming)
 *
 * Created by rajivprab on 7/20/17.
 */
public abstract class Subscription {
    public abstract String getHeader();
    public abstract String getHeaderUrl();
    public abstract int getNumStories();
    public abstract String getContent(Instant start, Instant end);

    String getTopContent(Collection<SocialPost> unsortedPosts) {
        return getTopContent(unsortedPosts, Instant.MIN, Instant.MAX);
    }

    String getTopContent(Collection<SocialPost> unsortedPosts, Instant start, Instant end) {
        return getContentPresorted(SocialPost.getTop(unsortedPosts, start, end));
    }

    String getSpeechlessContent(Collection<SocialPost> unsortedPosts, Instant start, Instant end) {
        return getContentPresorted(SocialPost.getSpeechless(unsortedPosts, start, end));
    }

    String getBuzzingContent(Collection<SocialPost> unsortedPosts, Instant start, Instant end) {
        return getContentPresorted(SocialPost.getBuzzing(unsortedPosts, start, end));
    }

    // ----------------

    private String getContentPresorted(List<SocialPost> sortedPosts) {
        if (sortedPosts.isEmpty()) {
            Toolbox.logger().report(this, Severity.WARN, "No posts found in " + getHeader());
            return "No posts found. Did you make a typo? Fill out the form again if so.";
        }
        List<String> entries = sortedPosts.stream()
                                    .limit(getNumStories())
                                    .map(Subscription::getEntry)
                                    .collect(ImmutableList.toImmutableList());
        return Joiner.on("<br>\n").join(entries);
    }

    private static String getEntry(SocialPost post) {
        String entryWithoutCommentsUrl =
                String.format("[%d] %s", post.getScoreDisplay(), getHtmlLink(post.getTitle(), post.getUrl()));
        if (post.getUrl().equals(post.getCommentsUrl())) {
            return entryWithoutCommentsUrl;
        }
        return entryWithoutCommentsUrl +
                " - <i><small>" + getHtmlLink("comments", post.getCommentsUrl()) + "</small></i>";
    }
}
