package com.rajivprab.daily_reddit.subscription;

import com.rajivprab.daily_reddit.clients.NYTClient;

import java.time.Instant;

public class NYTSubscription extends Subscription {
    private final int numStories;

    // ------ Constructors -------

    public static final NYTSubscription INSTANCE = new NYTSubscription(30);

    public static NYTSubscription getMiniInstance() {
        return new NYTSubscription(5);
    }

    private NYTSubscription(int numStories) {
        this.numStories = numStories;
    }

    // -------- Functionality ------

    @Override
    public String getHeader() {
        return "New York Times";
    }

    @Override
    public String getHeaderUrl() {
        return "https://www.nytimes.com/trending/";
    }

    @Override
    public int getNumStories() {
        return numStories;
    }

    // NYTimes published most articles at the same time in the morning,
    // so just get the previous day's most popular posts
    @Override
    public String getContent(Instant start, Instant end) {
        return getTopContent(NYTClient.MOST_POPULAR.subList(0, 20));
    }
}
