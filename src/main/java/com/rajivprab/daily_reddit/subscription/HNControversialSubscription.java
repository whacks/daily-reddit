package com.rajivprab.daily_reddit.subscription;

import com.rajivprab.daily_reddit.clients.HNClient;

import java.time.Instant;

/**
 * A subscription to HN, along with the number of stories desired daily
 *
 * Created by rajivprab on 7/20/17.
 */
public class HNControversialSubscription extends Subscription {
    private final int numStories;

    public HNControversialSubscription(int numStories) {
        this.numStories = numStories;
    }

    @Override
    public String getHeader() {
        return "Hacker News - Controversial";
    }

    @Override
    public String getHeaderUrl() {
        return "https://news.ycombinator.com";
    }

    @Override
    public int getNumStories() {
        return numStories;
    }

    @Override
    public String getContent(Instant start, Instant end) {
        return getBuzzingContent(HNClient.getPosts(), start, end);
    }

    // ----------- Auto-generated ----------------

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HNControversialSubscription)) return false;

        HNControversialSubscription that = (HNControversialSubscription) o;

        return numStories == that.numStories;
    }

    @Override
    public int hashCode() {
        return numStories;
    }
}
