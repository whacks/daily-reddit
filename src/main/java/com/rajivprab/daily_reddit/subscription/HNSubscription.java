package com.rajivprab.daily_reddit.subscription;

import com.rajivprab.daily_reddit.clients.HNClient;

import java.time.Instant;

/**
 * A subscription to HN, along with the number of stories desired daily
 *
 * Created by rajivprab on 7/20/17.
 */
public class HNSubscription extends Subscription {
    private final int numStories;

    public HNSubscription(int numStories) {
        this.numStories = numStories;
    }

    @Override
    public String getHeader() {
        return "Hacker News - Popular";
    }

    @Override
    public String getHeaderUrl() {
        return "https://news.ycombinator.com";
    }

    @Override
    public int getNumStories() {
        return numStories;
    }

    @Override
    public String getContent(Instant start, Instant end) {
        return getTopContent(HNClient.getPosts(), start, end);
    }

    // ----------- Auto-generated ----------------

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HNSubscription)) return false;

        HNSubscription that = (HNSubscription) o;

        return numStories == that.numStories;
    }

    @Override
    public int hashCode() {
        return numStories;
    }
}
