package com.rajivprab.daily_reddit.subscription;

import com.google.common.base.Strings;
import com.rajivprab.daily_reddit.clients.RedditClient;
import com.rajivprab.daily_reddit.clients.RedditClient.SubDoesNotExistException;
import com.rajivprab.daily_reddit.clients.RedditClient.SubIsPrivateException;
import com.rajivprab.daily_reddit.clients.RedditClient.TopDuration;
import com.rajivprab.daily_reddit.clients.RedditClient.UnknownSubFetchError;
import com.rajivprab.daily_reddit.content.Digest;
import com.rajivprab.daily_reddit.services.Toolbox;
import org.apache.commons.lang3.Validate;
import org.rajivprab.sava.logging.Severity;

import java.time.Instant;

/**
 * Created by rajivprab on 7/20/17.
 */
public class RedditSubscription extends Subscription {
    private final String sub;
    private final int numStories;

    public RedditSubscription(String sub, int numStories) {
        Validate.isTrue(!Strings.isNullOrEmpty(sub), "Cannot create subscription with empty subreddit");
        this.sub = sub;
        this.numStories = numStories;
    }

    @Override
    public String getHeader() {
        return "/r/" + sub;
    }

    @Override
    public String getHeaderUrl() {
        return RedditClient.BASE_URL + "/r/" + sub;
    }

    @Override
    public int getNumStories() {
        return numStories;
    }

    @Override
    public String getContent(Instant start, Instant end) {
        try {
            return getTopContent(RedditClient.getTopPosts(sub, TopDuration.WEEK, 200), start, end);
        } catch (SubDoesNotExistException e) {
            Toolbox.logger().report(Digest.class, Severity.WARN, "Sub does not exist: /r/" + sub);
            return "Sub does not exist. Fill out the form again to fix this error.";
        } catch (SubIsPrivateException e) {
            Toolbox.logger().report(Digest.class, Severity.WARN, "Sub is private: /r/" + sub);
            return "Sub is private. Unable to access it. Fill out the form again to remove it from your list.";
        } catch (UnknownSubFetchError e) {
            Toolbox.logger().report(Digest.class, Severity.ERROR, "Unknown error fetching /r/" + sub, e);
            return "Unable to access sub.";
        }
    }

    @Override
    public String toString() {
        return "RedditSubscription{" + sub + '}';
    }

    // Convert to lower case to ensure that each sub's content is only cached once in Digest.SUB_TO_CONTENT
    // getRankedContent() is not affected by sub.case anyway
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RedditSubscription)) return false;

        RedditSubscription that = (RedditSubscription) o;

        if (numStories != that.numStories) return false;
        return sub.toLowerCase().equals(that.sub.toLowerCase());
    }

    @Override
    public int hashCode() {
        int result = sub.toLowerCase().hashCode();
        result = 31 * result + numStories;
        return result;
    }
}
