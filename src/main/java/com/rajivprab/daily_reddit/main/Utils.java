package com.rajivprab.daily_reddit.main;

import com.google.common.collect.Lists;
import com.rajivprab.daily_reddit.content.Post;
import com.rajivprab.daily_reddit.content.SocialPost;
import com.rajivprab.daily_reddit.services.Toolbox;
import org.apache.logging.log4j.Logger;
import org.rajivprab.cava.ThreadUtilc;
import org.rajivprab.cava.Validatec;

import java.time.Duration;
import java.time.LocalTime;
import java.util.*;
import java.util.Map.Entry;

public class Utils {
    private static final Logger log = Toolbox.log(Utils.class);
    private static final Random RNG = new Random();

    public static void randomSleep(Duration max) {
        Duration sleep = Duration.ofMillis(RNG.nextInt(Math.toIntExact(max.toMillis())));
        log.info("Sleeping for: " + sleep);
        ThreadUtilc.sleep(sleep);
    }

    private static final String TRUNCATE_SUFFIX = " (cont)";

    static <T> T getWeightedRandom(Map<T, Integer> weights) {
        Validatec.equals(weights.values().stream().mapToInt(i -> i).sum(), 100);

        int target = RNG.nextInt(100) + 1;
        Validatec.greaterOrEqual(target, 1);
        Validatec.greaterOrEqual(100, target);

        int entryIndex = 0;
        for (Entry<T, Integer> entry : weights.entrySet()) {
            entryIndex += entry.getValue();
            if (target <= entryIndex) { return entry.getKey(); }
        }

        throw new IllegalStateException("Target: " + target);
    }

    static String trim(String title, int maxLength) {
        Validatec.greaterThan(maxLength, 10);
        title = title.trim();
        if (title.length() <= maxLength) { return title; }

        int maxLengthBeforeTruncatedSuffix = maxLength - TRUNCATE_SUFFIX.length();
        return title.substring(0, maxLengthBeforeTruncatedSuffix) + TRUNCATE_SUFFIX;
    }

    public static <T> List<T> truncate(List<T> list, int maxSize) {
        return list.size() > maxSize ? list.subList(0, maxSize) : list;
    }

    public static <T> T getRandom(List<T> list) {
        Validatec.notEmpty(list);
        return list.get(RNG.nextInt(list.size()));
    }

    static boolean sleepTime(LocalTime startTime, LocalTime endTime) {
        LocalTime time = LocalTime.now(TimeZone.getTimeZone("America/New_York").toZoneId());
        return time.isBefore(startTime) || time.isAfter(endTime);
    }

    static boolean shouldPost(int numPostsPerDay, int numIterationsPerDay) {
        double probabilityThreshold = numPostsPerDay * 1.0 / numIterationsPerDay;
        log.info("Starting probability check for threshold: " + probabilityThreshold);
        return RNG.nextDouble() < probabilityThreshold;
    }

    static Post getNewTopRandom(List<SocialPost> posts, Collection<Post> alreadyPosted) {
        posts = Lists.newArrayList(posts);
        posts.removeAll(alreadyPosted);

        posts = SocialPost.getTop(posts);

        int topSize = 3;
        Validatec.greaterOrEqual(posts.size(), topSize, "Found posts: " + posts);
        return Utils.getRandom(posts);
    }
}
