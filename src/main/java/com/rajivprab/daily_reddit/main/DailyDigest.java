package com.rajivprab.daily_reddit.main;

import com.rajivprab.daily_reddit.content.Digest;
import com.rajivprab.daily_reddit.subscription.Subscription;
import com.rajivprab.daily_reddit.services.Toolbox;
import com.rajivprab.daily_reddit.services.UserSignUps;
import org.apache.commons.lang3.Validate;
import org.rajivprab.cava.Validatec;
import org.rajivprab.sava.logging.Severity;

import java.util.Collection;

/**
 * Top level main method
 * <p>
 * Created by rajivprab on 5/19/17.
 */
public class DailyDigest {
    public static void main(String... args) {
        try {
            Toolbox.log(DailyDigest.class).info("Starting DailyDigest");
            run();
            Toolbox.log(DailyDigest.class).info("DailyDigest completed successfully");
        } catch (Throwable e) {
            Toolbox.logger().report(DailyDigest.class, Severity.ERROR, "Exception in main", e);
            throw e;
        }
    }

    private static void run() {
        UserSignUps signUps = UserSignUps.get();
        Validatec.notEmpty(signUps.getUsers(), "Found no user-signups at all");
        boolean allEmailsSent = true;
        for (String email : signUps.getUsers()) {
            // First send email to curUser, and then check if we had a previous failure
            allEmailsSent = run(email, signUps.getSubs(email)) && allEmailsSent;
        }
        Validate.isTrue(allEmailsSent, "Some emails not sent successfully");
    }

    static boolean run(String email, Collection<Subscription> subs) {
        String body = Digest.generate(subs);
        String name = email.split("@")[0];
        boolean success = Toolbox.getEmail().sendEmail(email, name, body);
        if (!success) {
            Toolbox.logger().report(DailyDigest.class, Severity.ERROR,
                                    "Unable to send to user: " + email + ", with subs: " + subs);
        }
        return success;
    }
}
