package com.rajivprab.daily_reddit.main;

import com.rajivprab.daily_reddit.services.Toolbox;

/**
 * Purely for HerokuWeb
 *
 * Created by rajivprab on 6/15/17.
 */
public class Dummy {
    public static void main(String... args) {
        Toolbox.log(Dummy.class).info("Calling Dummy");
    }
}
