package com.rajivprab.daily_reddit.services;

import org.rajivprab.sava.email.EmailInfo;
import org.rajivprab.sava.email.SendGrids;

/**
 * Client for sending emails to users.
 *
 * Keep url-tracking disabled in SendGrid console, in order to reduce email size by ~70%.
 * Without it, we easily run into the gmail message-size limit, resulting in clipping.
 *
 * Created by rajivprab on 5/19/17.
 */
public class EmailProj {
    static final EmailProj EMAIL = new EmailProj();

    private static final int DAILY_DIGEST_SUPPRESSION_GROUP = 9915;

    private EmailProj() {}

    public boolean sendEmail(String toEmail, String name, String body) {
        body += "<hr class=\"dash\" />";
        EmailInfo info = EmailInfo.builder()
                                  .setToEmail(toEmail)
                                  .setToName(name)
                                  .setTitle("Daily Digest")
                                  .setBody(body)
                                  .setFromEmail("admin@thecaucus.net")
                                  .setFromName("Daily-Digest")
                                  .setBccEmail("thecaucusnet+reddit_daily_digest@gmail.com")
                                  .setReplyToEmail("thecaucusnet+reddit_daily_digest@gmail.com")
                                  .setSuppressionGroup(DAILY_DIGEST_SUPPRESSION_GROUP)
                                  .build();
        return Holder.EMAIL_CLIENT.sendEmail(info);
    }

    private static class Holder {
        private static final String API_KEY = Toolbox.getApiKeys("sendgrid/sendgrid.json")
                                                     .getString("api_key");
        static final SendGrids EMAIL_CLIENT = SendGrids.client(API_KEY);
    }
}
