package com.rajivprab.daily_reddit.services;

import com.google.common.collect.ImmutableMap;
import org.rajivprab.cava.DynamicConstant;
import org.rajivprab.sava.events.Publisher;
import org.rajivprab.sava.logging.Dispatcher;
import org.rajivprab.sava.logging.Severity;

import java.util.Map;

/**
 * Bridge between LogDispatcher and SNSPublisher
 *
 * Created by rajivprab on 6/11/17.
 */
class DispatcherProj implements Dispatcher {
    static final DynamicConstant<Dispatcher> DISPATCHER =
            DynamicConstant.lazyDefault(() -> new DispatcherProj(Toolbox.getPublisher()));

    static final String TOPIC_ERROR = "arn:aws:sns:us-east-1:444813972869:caucus_error";

    private static final String TOPIC_WARNING = "arn:aws:sns:us-east-1:444813972869:caucus_warning";
    private static final String TOPIC_FATAL = "arn:aws:sns:us-east-1:444813972869:caucus_fatal";

    private static final Map<Severity, String> SEVERITY_TOPIC = ImmutableMap.of(
            Severity.WARN, TOPIC_WARNING,
            Severity.ERROR, TOPIC_ERROR,
            Severity.FATAL, TOPIC_FATAL);

    private final Publisher publisher;

    private DispatcherProj(Publisher publisher) {
        this.publisher = publisher;
    }

    @Override
    public void dispatch(Severity severity, String title, String message) {
        if (!SEVERITY_TOPIC.containsKey(severity)) { return; }
        publisher.publish(SEVERITY_TOPIC.get(severity), title, message);
    }
}
