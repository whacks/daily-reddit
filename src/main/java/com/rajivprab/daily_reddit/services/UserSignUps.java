package com.rajivprab.daily_reddit.services;

import com.google.common.collect.*;
import com.rajivprab.daily_reddit.subscription.*;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONObject;
import org.rajivprab.cava.IOUtilc;
import org.rajivprab.cava.JsonUtilc;
import org.rajivprab.cava.Validatec;
import org.rajivprab.cava.exception.CheckedExceptionWrapper;
import org.rajivprab.sava.logging.Severity;

import javax.ws.rs.core.Response.Status;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * All user-configs, such as their email addresses and sub-signups
 * <p>
 * Created by rajivprab on 5/21/17.
 */
public class UserSignUps {
    private static final String REDDIT_SIGNUP = "Reddit";
    private static final String NYTIMES_SIGNUP = "New York Times";
    private static final String DEPRECATED_HACKER_NEWS_SIGNUP = "Hacker News";
    private static final String HACKER_NEWS_POPULAR_SIGNUP = "Hacker News - Popular";
    private static final String HACKER_NEWS_INSIGHTFUL_SIGNUP = "Hacker News - Insightful";

    private static final Collection<String> VALID_SIGNUPS =
            ImmutableList.of(REDDIT_SIGNUP, NYTIMES_SIGNUP,
                             DEPRECATED_HACKER_NEWS_SIGNUP, HACKER_NEWS_INSIGHTFUL_SIGNUP, HACKER_NEWS_POPULAR_SIGNUP);

    private final Multimap<String, Subscription> emailToSubs;

    public static UserSignUps get() {
        try {
            HttpGet request = new HttpGet(new URIBuilder(CredentialsHolder.PATH)
                                                  .addParameter("key", CredentialsHolder.KEY).build());
            HttpResponse response = HttpClients.createDefault().execute(request);
            Validatec.equals(response.getStatusLine().getStatusCode(), Status.OK.getStatusCode());
            return new UserSignUps(new JSONObject(IOUtilc.toString(response.getEntity().getContent())));
        } catch (Exception e) {
            Toolbox.logger().report(UserSignUps.class, Severity.FATAL, e);
            throw CheckedExceptionWrapper.wrapIfNeeded(e);
        }
    }

    UserSignUps(JSONObject formResponses) {
        Toolbox.log(this).info("Received FormResponses:\n" + formResponses.toString(2));
        Multimap<String, Subscription> result = ArrayListMultimap.create();
        JsonUtilc.<JSONObject>getStream(formResponses.getJSONArray("responses"))
                .forEach(response -> parseResponse(response, result));
        Validatec.greaterThan(1000, result.keySet().size(), "Spam? Seeing more than a 1000 unique users");
        this.emailToSubs = ImmutableMultimap.copyOf(result);
    }

    private static void parseResponse(JSONObject response, Multimap<String, Subscription> result) {
        String email = response.getString("email").trim();
        if (!isValidEmail(email)) {
            Toolbox.logger().report(UserSignUps.class, Severity.WARN, "Getting bad email address: " + email);
            return;
        }
        List<Subscription> subscriptions = Lists.newArrayList();
        if (sitesIncludes(response, NYTIMES_SIGNUP)) {
            subscriptions.add(NYTSubscription.INSTANCE);
        }
        if (sitesIncludes(response, HACKER_NEWS_INSIGHTFUL_SIGNUP) ||
                sitesIncludes(response, DEPRECATED_HACKER_NEWS_SIGNUP)) {
            subscriptions.add(new HNSpeechlessSubscription(10));
        }
        if (sitesIncludes(response, HACKER_NEWS_POPULAR_SIGNUP) ||
                sitesIncludes(response, DEPRECATED_HACKER_NEWS_SIGNUP)) {
            subscriptions.add(new HNSubscription(10));
        }
        subscriptions.addAll(getSubReddits(response));
        Validatec.notEmpty(subscriptions);
        result.removeAll(email);
        result.putAll(email, subscriptions);
    }

    private static boolean sitesIncludes(JSONObject response, String site) {
        return JsonUtilc.<String>getStream(response.getJSONArray("sites"))
                .peek(cur -> Validatec.contains(VALID_SIGNUPS, cur))
                .anyMatch(cur -> cur.equals(site));
    }

    private static Collection<Subscription> getSubReddits(JSONObject response) {
        String subredditsFormResponse = response.getString("subreddits");
        String subs = subredditsFormResponse.trim()
                                            .replaceAll("\n", " ")
                                            .replaceAll(" +", " ");
        if (subs.isEmpty()) {
            if (sitesIncludes(response, REDDIT_SIGNUP)) {
                subs = "WholesomeMemes HumansBeingBros UpliftingNews TIFU All PersonalFinance TED AskHistorians " +
                        "Philosophy Economics Psychology ShortScaryStories NoSleep";
            } else {
                return Lists.newArrayList();
            }
        }
        Validatec.matches(subs, "[0-9a-zA-Z _]+", "Expecting only alphanumeric, underscores, spaces. Found: " + subs);
        Collection<Subscription> subsParsed = Arrays.stream(subs.split(" "))
                                                    .map(sub -> new RedditSubscription(sub, 10))
                                                    .distinct()
                                                    .collect(Collectors.toList());
        Validatec.greaterThan(100, subsParsed.size(),
                              "Spam? Seeing more than a hundred subs for: " + subredditsFormResponse);
        return subsParsed;

    }

    static boolean isValidEmail(String email) {
        return email.matches(".+\\@.+\\..+");
    }

    public Collection<String> getUsers() {
        return emailToSubs.keySet();
    }

    public Collection<Subscription> getSubs(String user) {
        return emailToSubs.get(user);
    }

    private static class CredentialsHolder {
        private static final JSONObject CREDENTIALS =
                Toolbox.getApiKeys("google-apps-script-reddit-digest/credentials.json");

        static final String PATH = CREDENTIALS.getString("path");
        static final String KEY = CREDENTIALS.getString("key");
    }
}
