package com.rajivprab.daily_reddit.services;

import com.amazonaws.regions.Regions;
import org.json.JSONObject;
import org.rajivprab.cava.DynamicConstant;
import org.rajivprab.sava.events.Publisher;

/**
 * Events Publisher
 *
 * Created by rajivprab on 6/11/17.
 */
class PublisherProj {
    static final DynamicConstant<Publisher> PUBLISHER = DynamicConstant.lazyDefault(PublisherProj::createSNSPublisher);

    private static Publisher createSNSPublisher() {
        JSONObject credentials = Toolbox.getApiKeys("sns/sns_credentials.json");
        return Publisher.getSNS(credentials.getString("Access_Key_Id"),
                                credentials.getString("Secret_Access_Key"),
                                Regions.fromName(credentials.getString("Region")))
                        .reportErrorsToTopic(DispatcherProj.TOPIC_ERROR);
    }
}
