package com.rajivprab.daily_reddit.services;

import org.rajivprab.cava.DynamicConstant;
import org.rajivprab.sava.logging.LogDispatcher;

class LoggerProj {
    static final DynamicConstant<LogDispatcher> LOGGER =
            DynamicConstant.lazyDefault(() -> LogDispatcher.build(Toolbox.getDispatcher()));
}
