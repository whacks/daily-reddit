package com.rajivprab.daily_reddit.services;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.regions.Regions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.rajivprab.cava.JSONObjectImmutable;
import org.rajivprab.sava.events.Publisher;
import org.rajivprab.sava.keys.ApiKeysS3;
import org.rajivprab.sava.keys.ApiKeysSwitch;
import org.rajivprab.sava.logging.Dispatcher;
import org.rajivprab.sava.logging.LogDispatcher;
import org.rajivprab.sava.s3.AwsCredentials;
import org.rajivprab.sava.s3.S3Interface;

/**
 * Provides access to all 3rd party services
 * <p>
 * Created by rprabhakar on 12/28/15.
 */
public class Toolbox {
    public static void setQA(Publisher publisher) {
        ApiKeysHolder.API_KEYS.useQA();
        PublisherProj.PUBLISHER.set(publisher);
    }

    public static JSONObject getApiKeys(String s3Path) {
        return JSONObjectImmutable.build(getApiKeysRaw(s3Path));
    }
    private static String getApiKeysRaw(String s3Path) {
        return ApiKeysHolder.API_KEYS.getCredentials(s3Path);
    }

    public static EmailProj getEmail() {
        return EmailProj.EMAIL;
    }

    public static Logger log(Object object) {
        return log(object.getClass());
    }

    public static Logger log(Class<?> clazz) {
        return LogManager.getLogger(clazz);
    }

    public static LogDispatcher logger() {
        return LoggerProj.LOGGER.get();
    }

    public static Dispatcher getDispatcher() {
        return DispatcherProj.DISPATCHER.get();
    }

    static Publisher getPublisher() {
        return PublisherProj.PUBLISHER.get();
    }

    // ---------

    private static class ApiKeysHolder {
        private static final AWSCredentialsProvider CREDENTIALS_PROVIDER = AwsCredentials.customEnvironmentVariable(
                "PERSONAL_S3READONLY_ACCESS_KEY_ID",
                "PERSONAL_S3READONLY_SECRET_ACCESS_KEY");

        private static final S3Interface S3_INTERFACE = S3Interface.get(CREDENTIALS_PROVIDER, Regions.US_EAST_1);

        static final ApiKeysSwitch API_KEYS = ApiKeysSwitch.build(
                ApiKeysS3.build("rajivprab-credentials", S3_INTERFACE),
                ApiKeysS3.build("rajivprab-credentials-qa", S3_INTERFACE));
    }
}
