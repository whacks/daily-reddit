#!/usr/bin/env bash

# Run this manually, using prod S3 access keys
# heroku config:set PERSONAL_S3READONLY_ACCESS_KEY_ID={key_id}
# heroku config:set PERSONAL_S3READONLY_SECRET_ACCESS_KEY={access_key}

# If changing this, change mvn-surefire config in pom as well
# If changing stack-size (Xss), also change MEMORY_PER_THREAD_KB constant
heroku config:set JAVA_OPTS='-XX:+UseCompressedOops -XX:+DisableExplicitGC -XX:+UseConcMarkSweepGC -XX:CMSInitiatingOccupancyFraction=80 -Xss512k'

# Needed to support selenium
# heroku buildpacks:add heroku/google-chrome
# heroku buildpacks:add heroku/chromedriver