About
=====

Daily emails containing the top posts from your favorite aggregators.

Screenshot
==========
<img src="src/main/resources/email-screenshot-4.png" width=816 height=538>

How Do I Sign Up?
=================

Fill out [this form](https://docs.google.com/forms/d/e/1FAIpQLSfyBQ_wEHbh00CcLotTIFQn9VwnxyoXjqkoKVO8OsP-zI2Mtg/viewform?usp=sf_link#response=ACYDBNiUtd-pQ2W2Ss4GJx8ivQT-D7yzRa9lWhpBykoeyzKJDBw8w_vHlGJxeeI) with your email address and list of favorite aggregators.

FAQ
===

**How do I update my list of subreddits?**

Just fill out the form again with your updated list. You will only be sent one email per day, no matter how many times you fill out the form. All previous responses with the same email address will be ignored.

**How do I unsubscribe?**

Just click on the unsubscribe link in your email.

**Where are you getting the "insightful" rankings from?**

That's a secret. Though if you really want to know, I suppose I can't stop you from [snooping through the source code](https://gitlab.com/whacks/daily-reddit/blob/master/src/main/java/com/rajivprab/daily_reddit/content/Comparators.java#L18-20).

**Why are many of the articles a day old?**

We need to do this to ensure that you don't miss out on any quality content, while also ensuring that each day's digest is unique. You can read more about this [here](https://gitlab.com/whacks/daily-reddit/blob/master/src/main/java/com/rajivprab/daily_reddit/content/Digest.java#L20-27).

**Can I run this myself?**

Sure. Feel free to fork or use this project as a dependency, call the Digest.generate method, and output the resulting content in any way you'd like.

**Can you add support for {XYZ} aggregator?**

I can certainly look into it, if it has an easily accessible API, and if it's sufficiently popular. Pull requests are welcome too.