Commands
========
Heroku dashboard is [here](https://dashboard.heroku.com/apps/my-daily-reddit).

- System dependencies:
  - [JDK 11](https://stackoverflow.com/a/28635465/4816322)
  - [ChromeDriver](https://github.com/SeleniumHQ/selenium/wiki/ChromeDriver)
    - For errors regarding specific chrome-versions, try updating ChromeDriver
    - Mac: `brew cask upgrade chromedriver`
  - [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli#download-and-install)
  

Remote repositories:
- daily-digest
  - ```git remote add daily-digest git@gitlab.com:whacks/daily-reddit.git```
  - ```git pull daily-digest master:daily-digest-master```
- heroku
  - ```git remote add heroku https://git.heroku.com/my-daily-reddit.git```

Pushing to gitlab: ```git push {daily-digest} {local-branch}:master```

Deploying: ```git push heroku master && ./heroku_config.sh```

Running Integration tests: ```mvn clean install -DrunIT```
- Many email-related integration tests will only pass if you have the S3 credentials in your env
- Check email after running integration tests, to verify that the generated email was successfully sent, looks fine visually, and has clickable links


Technology Stack
================
- Heroku scheduler that runs the application on a daily basis
- [Google forms](https://docs.google.com/forms/d/1SuIwrIzgaCQQJpTJRTL4ocmhBnkWR1DmtgTBdfbA0ds/edit) to collect and store sign-ups
- [Google script editor](https://script.google.com/d/Mpqp6wTFhsWLsrIo9Tf30r-VGmcMBgjNE/edit?mid=ACjPJvEYKc9pHE9em-b5Oeu_Qed2lGOeXRuOpOcQjp3IcIGbR-SYuU8hZMIvoZzylAxh0TSY1LOfsgzn3_Rlh4frwKyOTqhOAoS8kIDQF9SugYuGptRbFqEtl_ZE7sitGQlT-hNpfIRn7A&uiv=2) to programmatically fetch the form-responses
- SendGrid emailer
- AWS SNS for ops alerts
- Credentials for above stored in AWS S3


TODOs
=====
- Nothing major


Nice to Haves
=============
- Allow users to specify for each source, the number of posts they want to receive per day
  - Problem: Google Form's grid-format [only allows for check-boxes or radio buttons, not text fields](https://productforums.google.com/forum/#!topic/docs/2HFVKlsn2rQ)
  - All back-end support has already been added. Only need an updated form that can handle this input
- Form could use a checklist for subreddit-signup, and not a single text field
  - Problem: Google forms doesn't allow users to add multiple new fields
- User specific configs, like how often they would like to receive the email, and at what time.
- Store the results of the previous day's digest, and change the filter algorithm to get all top results from the last 2 days that were not included in yesterday's digest. This way, users will get more timely content.
